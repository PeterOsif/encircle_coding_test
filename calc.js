// ****************************************************************************************************************************************
// Following from node.js process agrument documentation: https://nodejs.org/docs/latest/api/process.html#process_process_argv
// Our input will be in the process.argv[2]:
// ****************************************************************************************************************************************
const input = process.argv[2] || "";

// ****************************************************************************************************************************************
// calcExpression - to calculate an individual expression and identifly an add or mutiply command and send remaining array to the
// corresponding array.reduce functions. Additionally, a map command is used to replace string digits as integers
// ****************************************************************************************************************************************
const calcExpression = (expressionArray) => {
  if (expressionArray.slice(0, 3).join("") === "add") {
    return add(expressionArray.slice(3, expressionArray.length).map((x) => +x));
  } else if (expressionArray.slice(0, 8).join("") === "multiply") {
    return multiply(
      expressionArray.slice(8, expressionArray.length).map((x) => +x)
    );
  } else {
    return {
      Error:
        "Looks like the expression is bad, can you double check your input?",
    };
  }
};

// ****************************************************************************************************************************************
// runClac - recursive function to run calculations
// ****************************************************************************************************************************************
const runCalc = (inputArray) => {
  // Need to get the first instance of an expression within the parenthesis ()
  const closingIndex = inputArray.indexOf(")");
  const startingIndex = inputArray.lastIndexOf("(");

  // Calculate the first instance of an expression
  const calcArray = inputArray.slice(startingIndex + 1, closingIndex);
  const calcCurrent = calcExpression(calcArray);

  // Replace this first instance of an expression of the original inputArray with the most recent calculation
  inputArray.splice(
    startingIndex,
    closingIndex - startingIndex + 1,
    calcCurrent
  );

  // If we still have expressions to calculate then run runCalc again with the new inputArray otherwise we're done!
  if (
    inputArray.indexOf(")") > 0 &&
    inputArray.length > 0 &&
    !calcCurrent.Error
  ) {
    return runCalc(inputArray);
  } else {
    return calcCurrent;
  }
};

// ****************************************************************************************************************************************
// Use Array Reduce: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
// to perform cumulative functions on each array item.  Note use of arrays will allow us to use larger data sets (i.e. add 2 2 2 2 ....)
// ****************************************************************************************************************************************
const add = (variableArray) => {
  return variableArray.reduce((a, b) => a + b);
};

const multiply = (variableArray) => {
  return variableArray.reduce((a, b) => a * b);
};

// ****************************************************************************************************************************************
// processInput - clean input string and make usable array for calculations
// ****************************************************************************************************************************************
const processInput = (inputString) => {
  let output = 0;
  const cleanInput = inputString.replace(/\s/g, "").toLowerCase(); // Remove spaces/breaks from inputString
  const inputArray = [...cleanInput]; // Creates a character array of the inputed expression

  // If we have an identifier of an expression parenthesis () then run the calculation other return the value given
  if (inputArray.indexOf(")") > 0) {
    output = runCalc(inputArray);
  } else {
    output = inputString;
  }

  console.log("output", output);
};

// Initiate calculations
processInput(input);

module.exports = {
  calcExpression,
  add,
  multiply,
};
