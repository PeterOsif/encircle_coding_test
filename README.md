# encircle_coding_test

Coding test for encircle development position

## How to run the calculator

To run calc.js you will need to run `node` + `calc.js` + `(express var var)`, please note that the expression will need to be a string following this example:

```
> node calc.js '(multiply 2 (add (multiply 2 3) 8))'
```

## How to run the unit test

This project uses `jest` as a unit testing library, to get setup run:

```
> npm install
```

and to run the tests

```
> npm run test
```
