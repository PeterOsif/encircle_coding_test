const calc = require("./calc.js");

// add

test("two simple add values and resolve to correct values", () => {
  expect(calc.add([2, 2])).toBe(4);
});

test("mutiple simple add values and resolve to correct values", () => {
  expect(calc.add([2, 2, 2, 2, 2])).toBe(10);
});

test("add zero values and resolve to correct values", () => {
  expect(calc.add([2, 0, 0])).toBe(2);
});

// multiply

test("two simple multiply values and resolve to correct values", () => {
  expect(calc.multiply([2, 3])).toBe(6);
});

test("mutiple simple multiply values and resolve to correct values", () => {
  expect(calc.multiply([2, 2, 2, 2, 2])).toBe(32);
});

test("mutiple zero multiply values and resolve to 0", () => {
  expect(calc.multiply([2, 2, 2, 2, 0])).toBe(0);
});

// calcExpression

test("calcExpress works for add two values", () => {
  expect(calc.calcExpression(["a", "d", "d", "2", "2"])).toBe(4);
});

test("calcExpress works for add multiple values", () => {
  expect(calc.calcExpression(["a", "d", "d", "2", "2", "2", "2"])).toBe(8);
});

test("calcExpress works for add integer values", () => {
  expect(calc.calcExpression(["a", "d", "d", 2, 2, 2, 2])).toBe(8);
});

test("calcExpress works for add mix string & integer values", () => {
  expect(calc.calcExpression(["a", "d", "d", "2", 2, "2", 2])).toBe(8);
});

test("calcExpress works for multipy two values", () => {
  expect(
    calc.calcExpression(["m", "u", "l", "t", "i", "p", "l", "y", "2", "3"])
  ).toBe(6);
});

test("calcExpress works for multipy multiple values", () => {
  expect(
    calc.calcExpression([
      "m",
      "u",
      "l",
      "t",
      "i",
      "p",
      "l",
      "y",
      "2",
      "2",
      "2",
      "2",
    ])
  ).toBe(16);
});

test("calcExpress works for multipy integer values", () => {
  expect(
    calc.calcExpression(["m", "u", "l", "t", "i", "p", "l", "y", 2, 2, 2, 2])
  ).toBe(16);
});

test("calcExpress works for multipy mix string & integer values", () => {
  expect(
    calc.calcExpression([
      "m",
      "u",
      "l",
      "t",
      "i",
      "p",
      "l",
      "y",
      "2",
      2,
      "2",
      2,
    ])
  ).toBe(16);
});
